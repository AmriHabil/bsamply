<!DOCTYPE html>
<html data-wf-page="5cd0a4dfc94507558498109b" data-wf-site="5cc08428d2c73015d3b08896">
<head>
    <meta charset="utf-8">
    <title>Registration</title>
    <meta content="Registration" property="og:title">
    <meta content="width=device-width, initial-scale=1" name="viewport">
    <link href="{{asset('assets/css/normalize.css')}}" rel="stylesheet" type="text/css">
    <link href="{{asset('assets/css/webflow.css')}}" rel="stylesheet" type="text/css">
    <link href="{{asset('assets/css/platform.css')}}" rel="stylesheet" type="text/css">
    <script src="https://ajax.googleapis.com/ajax/libs/webfont/1.6.26/webfont.js" type="text/javascript"></script>
    <script type="text/javascript">WebFont.load({  google: {    families: ["Lato:100,100italic,300,300italic,400,400italic,700,700italic,900,900italic"]  }});</script>
    <!-- [if lt IE 9]><script src="https://cdnjs.cloudflare.com/ajax/libs/html5shiv/3.7.3/html5shiv.min.js" type="text/javascript"></script><![endif] -->
    <script type="text/javascript">!function(o,c){var n=c.documentElement,t=" w-mod-";n.className+=t+"js",("ontouchstart"in o||o.DocumentTouch&&c instanceof DocumentTouch)&&(n.className+=t+"touch")}(window,document);</script>
    <link href="https://daks2k3a4ib2z.cloudfront.net/img/favicon.ico" rel="shortcut icon" type="image/x-icon">
    <link href="https://daks2k3a4ib2z.cloudfront.net/img/webclip.png" rel="apple-touch-icon">
    <style>
        .w-select {
            background-image: none;
            background-image: none;
            border-radius: 0px; !important
        }
        #popup{
            padding: 18px
        }

        #popup p, #popup p.more{
            line-height: inherit
        }
        .text-danger{
            color: red;
        }
    </style>
</head>
<body>
<div id="overlay">
    <img src="{{asset('assets/images/closeWhite.svg')}}" width="24" height="24" id="close" alt="" class="close">
    <div id="popup">
        <p id="message">
        </p>
    </div>
</div>
<div class="_50 full-page">
    <div class="wrapper">
        <div class="search-filter-bar login w-clearfix">
            <h1 style="text-align:left;display:inline-block"><img src="{{asset('assets/images/logo.svg')}}" style="width:32px;margin-right:4px" alt="">Concorsi</h1>
            <h4>Benvenuto, registra il tuo account per procedere</h4>
            <form action="" id="register_from" >
                <div class="search-card w-form form-login">
                    <div class="text-field-container no-shadow">
                        <label class="label-field">Username</label>
                        <input type="text" class="search-field-contain w-input" maxlength="256" name="username" placeholder="Habil Amri" id="username">
                        <ul id="username_errors" class="registration_errors">

                        </ul>
                    </div>
                    <div class="text-field-container no-shadow">
                        <label class="label-field">Mail</label>
                        <input type="email" class="search-field-contain w-input" maxlength="256" name="email" placeholder="candidato@service.it" id="email">
                        <ul id="email_errors" class="registration_errors">

                        </ul>
                    </div>
                    <div class="text-field-container no-shadow">
                        <label class="label-field">Codice fiscale</label>
                        <input type="text" class="search-field-contain w-input" maxlength="16" minlength="16" name="cf" placeholder="CCCNNN00M11S222V" id="cf">
                        <ul id="cf_errors" class="registration_errors">

                        </ul>
                    </div>
                    <div class="text-field-container no-shadow">
                        <label class="label-field">Phone</label>
                        <input type="text" class="search-field-contain w-input numeric" maxlength="12" name="tel" placeholder="01212121212" id="tel">
                        <ul id="tel_errors" class="registration_errors">

                        </ul>
                    </div>
                    <div class="text-field-container no-shadow">
                        <label class="label-field">Password</label>
                        <input type="password" class="search-field-contain w-input "  name="password" placeholder="********" id="password">
                        <ul id="password_errors" class="registration_errors">

                        </ul>
                    </div>
                    <div class="text-field-container no-shadow">
                        <label class="label-field">Confirm Password</label>
                        <input type="password" class="search-field-contain w-input "  name="c_password" placeholder="********" id="c_password">
                        <ul id="c_password_errors" class="registration_errors">

                        </ul>
                    </div>
                </div>
                <a href="{{route('user.login')}}" class="button text-row no-margin w-button">Already registered?</a>
                <span id="error">
                </span>
                <input type="checkbox" id="auth" name="accept"> I accept the <a href="#" target="_blank">Privacy Policy</a>
                <ul id="accept_errors" class="registration_errors">

                </ul>
                <input value=" Sign in " type="submit" class="button login w-button">
            </form>


        </div>
    </div>
</div>
<div class="_50 full-page img"></div>
<script src="https://d1tdp7z6w94jbb.cloudfront.net/js/jquery-3.3.1.min.js" type="text/javascript" integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8=" crossorigin="anonymous"></script>
<script src="{{asset('assets/js/webflow.js')}}" type="text/javascript"></script>
<!-- [if lte IE 9]><script src="https://cdnjs.cloudflare.com/ajax/libs/placeholders/3.0.2/placeholders.min.js"></script><![endif] -->
<script>
    function submitForm(){
        if(document.getElementById("mail").value=="" || document.getElementById("mail").value.indexOf("@")==-1)
        {
            alert("È necessario inserire la mail");
            return;
        }
        if(!document.getElementById("auth").checked)
        {
            alert("È necessario accettare la privacy policy");
            return;
        }
        if(document.getElementById("cf").value.length!=16)
        {
            alert("Lunghezza codice fiscale non valida");
            return;
        }
        if( ! /^[0-9A-Z]{16}$/.test(document.getElementById("cf").value.toUpperCase()) )
        {
            alert("Il codice fiscale contiene valori non validi");
            return;
        }
        var s = 0;
        var even_map = "BAFHJNPRTVCESULDGIMOQKWZYX";
        for(var i = 0; i < 15; i++){
            var c = document.getElementById("cf").value[i].toUpperCase();
            var n = 0;
            if( "0" <= c && c <= "9" )
                n = c.charCodeAt(0) - "0".charCodeAt(0);
            else
                n = c.charCodeAt(0) - "A".charCodeAt(0);
            if( (i & 1) === 0 )
                n = even_map.charCodeAt(n) - "A".charCodeAt(0);
            s += n;
        }
        if( s%26 + "A".charCodeAt(0) !== document.getElementById("cf").value.toUpperCase().charCodeAt(15) )
        {
            alert("Codice fiscale non valido");
            return;
        }
        if(document.getElementById("tel").value.length<10)
        {
            alert("Inserire un numero di telefono valido");
            return;
        }
        if(!isNumeric(document.getElementById("tel").value))
        {
            alert("Inserire un numero di telefono valido");
            return;
        }
        //send user registration
    }


    function isNumeric(n){
        return !isNaN(parseFloat(n)) && isFinite(n);
    }

    $(document).ready(function(){
        $("#close").on("click", function(){
            $("#overlay").fadeOut();
        });
    })


    /*Habil Scripts*/
    $(document).ready(function () {
        /*Numeric verification*/
        $('.numeric').keyup(function(e){
            var max=$(this).attr('maxlenght');
            if (/\D/g.test(this.value))
            {
                // Filter non-digits from input value.
                this.value = this.value.replace(/\D/g, '');
            }
            if (e.which < 0x20) {
                // e.which < 0x20, then it's not a printable character
                // e.which === 0 - Not a character
                return; // Do nothing
            }
            if ($(this).val().toString().length == max) {
                e.preventDefault();
            } else if ($(this).val().toString().length > max) {
                // Maximum exceeded
                this.value = this.value.substring(0, max);
            }

        });
        /*Numeric verification*/
        $("#register_from").submit(function (e) {
            e.preventDefault();
            $('.registration_errors').html('');
            var Form=$(this)[0];
            var formData= new FormData(Form);
            $.ajax({
                type: 'POST',
                url:"{{route('user.create')}}",
                headers: {
                    'X-CSRF-TOKEN': '{{csrf_token()}}'
                },
                data: formData,
                processData: false,
                contentType: false,
                cache: false,
                success:function(data)
                {
                    if(data=='FAIL'){
                        Swal.fire({
                            title: "Fail!",
                            text: "CF has to be unique and is\n" +
                                "a 16 alphanumeric string always built in this way: ”\n" +
                                "LLLLLLNNLNNLNNNL” where L is a char and N is an\n" +
                                "integer.",
                            type: "error",
                            confirmButtonClass: "btn btn-confirm mt-2"
                        });
                    }else if(data.id){
                        Swal.fire({
                            title: "Good job!",
                            text: "You  are registred successfully. Your ID is: ".data.id,
                            type: "success",
                            confirmButtonClass: "btn btn-confirm mt-2"
                        });
                        Form.reset();
                    }


                }, error: function (reject) {

                    Swal.fire({
                        title: "Fail!",
                        text: "Please follow the instructions",
                        type: "error",
                        confirmButtonClass: "btn btn-confirm mt-2"
                    });
                    var response=$.parseJSON(reject.responseText);
                    $.each(response.errors,function(key,val){
                        $("#" + key ).addClass('is-invalid');
                        $("#" + key + "_errors").html('<li class="text-danger">'+val+'</li>');

                    });
                }
            });

        });
    })
    /*Habil Scripts*/

</script>
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/limonte-sweetalert2/11.4.26/sweetalert2.min.css" integrity="sha512-yq+qDDTUuLA4zvJjuvcpV809SNmM0ReTyeadKsNvW0cSvGVfj3K20SdlkburwJHHdzuGDtFElBcxndjd7J3nrQ==" crossorigin="anonymous" referrerpolicy="no-referrer" />
<script src="https://cdnjs.cloudflare.com/ajax/libs/limonte-sweetalert2/11.4.26/sweetalert2.min.js" integrity="sha512-lfP7VHOp6XS4CDxn82+BZ3narDVFMXjxy3yTQIcTxjpea9R77LM2VSWQn+qemnpR43d9+ogbMEfSc6OtzKQilA==" crossorigin="anonymous" referrerpolicy="no-referrer"></script>

</body>
</html>