<?php
    
    namespace App\Http\Controllers\Auth;
    
    
    
    
    use App\Jobs\SendEmailJob;
    use App\Mail\SendEmail;
    use App\Models\User;
    use App\Http\Controllers\Controller;
    use Carbon\Carbon;
    use Elasticquent\ElasticquentTrait;
    use Elasticsearch\ClientBuilder;
    use Illuminate\Support\Facades\Hash;
    use Illuminate\Support\Facades\Mail;
    use Illuminate\Support\Facades\Validator;
    use Illuminate\Http\Request;
    
    
    class RegisterController extends Controller
    {
        use ElasticquentTrait;
        /*
        |--------------------------------------------------------------------------
        | Register Controller
        |--------------------------------------------------------------------------
        |
        | This controller handles the registration of new users as well as their
        | validation and creation. By default this controller uses a trait to
        | provide this functionality without requiring any additional code.
        |
        */
       
        /**
         * Where to redirect users after registration.
         *
         * @var string
         */
        protected $redirectTo = '/home';
        
        /**
         * Create a new controller instance.
         *
         * @return void
         */
        public function __construct()
        {
            $this->middleware('guest');
            $this->middleware('guest:web');
            
        }
        
        /**
         * Get a validator for an incoming registration request.
         *
         * @param  array  $data
         * @return \Illuminate\Contracts\Validation\Validator
         */
        protected function validator(array $data)
        {
            
            $rules=[
                'cf' => 'required|string:16|unique:users',
                'username' => 'required|string|max:256|unique:users',
                'tel' => 'required|numeric:12|unique:users',
                'email' => 'required|email|unique:users',
                'password' => 'required|same:c_password',
                'c_password' => 'required|same:password',
                'accept' => 'required',
            ];
            $messages=[
                'cf.required' => 'È necessario inserire la cf!',
                'username.required' => 'È necessario inserire la username!',
                'tel.required' => 'È necessario inserire la tel!',
                'cf.max' => 'Lunghezza codice fiscale non valida!',
                'tel.size' => 'Lunghezza tel non valida! (12)',
                'cf.size' => 'Lunghezza tel non valida! (16)',
                'unique' => 'The :attribute should be  unique.',
                
            ];
            
            return Validator::make($data,$rules,$messages);
        }
        
        
        
        /**
         * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
         */
        public function showUserRegisterForm()
        {
            return view('register');
        }
        
        
        
        
        /**
         * @param Request $request
         *
         * @return \Illuminate\Http\RedirectResponse
         */
        protected function createUser(Request $request)
        {
            // Better way -> Requests/StoreUserRequest
            $this->validator($request->all())->validate();
            // Better way -> Requests/StoreUserRequest
            if (
                /*We Better use a trait */
                ctype_alpha(substr($request->cf,0,6))
                &&
                ctype_digit(substr($request->cf,6,2))
                &&
                ctype_alpha(substr($request->cf,8,1))
                &&
                ctype_digit(substr($request->cf,9,2))
                &&
                ctype_alpha(substr($request->cf,11,1))
                &&
                ctype_digit(substr($request->cf,12,3))
                &&
                ctype_alpha(substr($request->cf,15,1))
                /*We Better use a trait */
            ){
                $user=User::create([
                    'cf' => $request->cf,
                    'username' => $request->username,
                    'tel' => $request->tel,
                    'email' => $request->email,
                    'password' => Hash::make($request->password)


                ]);
                if(!isset($user->id)):
                    return 'FAIL';
                else:
    
                    /*SendEmailJob */
                dispatch(new SendEmailJob($user));
                    /*SendEmailJob */
                
                /*Add index for elasticsearch */
                $user->addToIndex();
                /*Add index for elasticsearch */
                return $user;
                endif;
            }
            
            return 'FAIL';
        }
    }